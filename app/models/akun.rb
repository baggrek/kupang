class Akun < ApplicationRecord
	validates :akun, uniqueness: {case_sensitive: false}
	validates_presence_of :name, :akun, :team
end
