class Absen < ApplicationRecord
  belongs_to :meet
  validates_presence_of :name
end
