class AbsensController < ApplicationController
	before_action :authorize

	def index
		@absen = Absen.all.order(name: :asc)
	end

	def create
		@meet = Meet.find(params[:meet_id])
		@absen = @meet.absens.create(absen_params)
		redirect_to meet_path(@meet)
	end

	def destroy
		@meet = Meet.find(params[:meet_id])
		@absen = @meet.absens.find(params[:id])
		@absen.destroy
		redirect_to meet_path(@meet)
	end

	private

	def absen_params
		params.require(:absen).permit(:name)
	end
end
