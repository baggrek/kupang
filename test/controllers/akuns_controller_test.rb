require 'test_helper'

class AkunsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @akun = akuns(:one)
  end

  test "should get index" do
    get akuns_url
    assert_response :success
  end

  test "should get new" do
    get new_akun_url
    assert_response :success
  end

  test "should create akun" do
    assert_difference('Akun.count') do
      post akuns_url, params: { akun: { akun: @akun.akun, name: @akun.name, team: @akun.team } }
    end

    assert_redirected_to akun_url(Akun.last)
  end

  test "should show akun" do
    get akun_url(@akun)
    assert_response :success
  end

  test "should get edit" do
    get edit_akun_url(@akun)
    assert_response :success
  end

  test "should update akun" do
    patch akun_url(@akun), params: { akun: { akun: @akun.akun, name: @akun.name, team: @akun.team } }
    assert_redirected_to akun_url(@akun)
  end

  test "should destroy akun" do
    assert_difference('Akun.count', -1) do
      delete akun_url(@akun)
    end

    assert_redirected_to akuns_url
  end
end
