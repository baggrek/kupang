require "application_system_test_case"

class MeetsTest < ApplicationSystemTestCase
  setup do
    @meet = meets(:one)
  end

  test "visiting the index" do
    visit meets_url
    assert_selector "h1", text: "Meets"
  end

  test "creating a Meet" do
    visit meets_url
    click_on "New Meet"

    fill_in "Pertemuan", with: @meet.pertemuan
    click_on "Create Meet"

    assert_text "Meet was successfully created"
    click_on "Back"
  end

  test "updating a Meet" do
    visit meets_url
    click_on "Edit", match: :first

    fill_in "Pertemuan", with: @meet.pertemuan
    click_on "Update Meet"

    assert_text "Meet was successfully updated"
    click_on "Back"
  end

  test "destroying a Meet" do
    visit meets_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Meet was successfully destroyed"
  end
end
