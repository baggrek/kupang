Rails.application.routes.draw do
	
  get 'sessions/new'
  get 'sessions/create'
  get 'sessions/destroy'
  resources :akuns
  resources :meets do
  	resources :absens
  end

  root 'meets#index'

  get '/signup' => 'users#new'
  post '/users' => 'users#create'
  get '/login' => 'sessions#new'
  post '/login' => 'sessions#create'
  get '/logout' => 'sessions#destroy'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
