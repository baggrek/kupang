class CreateAkuns < ActiveRecord::Migration[5.2]
  def change
    create_table :akuns do |t|
      t.string :name
      t.string :akun
      t.string :team

      t.timestamps
    end
  end
end
