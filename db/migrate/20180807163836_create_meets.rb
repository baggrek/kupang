class CreateMeets < ActiveRecord::Migration[5.2]
  def change
    create_table :meets do |t|
      t.string :pertemuan

      t.timestamps
    end
  end
end
