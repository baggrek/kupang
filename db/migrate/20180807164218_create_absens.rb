class CreateAbsens < ActiveRecord::Migration[5.2]
  def change
    create_table :absens do |t|
      t.string :name
      t.references :meet, foreign_key: true

      t.timestamps
    end
  end
end
